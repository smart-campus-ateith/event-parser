'''
This file is used as the driver of the external database.
'''

import os
from pymongo import MongoClient

class ExternalDatabase(object):
    def __init__(self):
        try:
            db_host = os.environ['EXTERNAL_DATABASE_HOST']
        except:
            print('external db host not found, setting to localhost')
            db_host = 'localhost'

        try:
            db_port = int(os.environ['EXTERNAL_DATABASE_PORT'])
        except:
            print('external db port not found, setting to 27017')
            db_port = 27017

        self.client = MongoClient(db_host, db_port)

    def store_value(self, data):
        database_name = os.environ['EXTERNAL_DATABASE_NAME']

        db = self.client[database_name]
        doc_id = db.projects.insert_one(data).inserted_id

        print('saved - id: ' + str(doc_id))
