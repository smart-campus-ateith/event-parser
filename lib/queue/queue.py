# TODO: use queue module https://docs.python.org/3.7/library/queue.html
# A simple queue for the proof of concept phase.

from model.event.event import Event
from datetime import datetime

class  QueueItem(object):
    def __init__(self, content, prev, next):
        time = datetime.now().timestamp()
        self.content = Event(content, time)
        self.prev = prev
        self.next = next

    def __repr__(self):
        return str(self.content)

    def isHead(self):
        return self.next is None

    def isTail(self):
        return self.prev is None

class Queue(object):
    def __init__(self):
        self.head = None
        self.tail = None

    def add(self, content):
        if self.head is None:
            item = QueueItem(content, None, None)
            self.head = self.tail = item
        else:
            item = QueueItem(content, self.tail, None)
            self.tail.next = item
            self.tail = item

    def pop(self):
        if self.head is None:
            raise Exception()

        item = self.head
        self.head = item.next
        return item

    def prepend(self, content):
        if self.head is None:
            self.add(content)
        else:
            item  = QueueItem(content, None, self.head)
            self.head = item

#
# if __name__ == '__main__':
#     q = Queue()
