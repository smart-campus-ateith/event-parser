"""
The application communicates using message objects.
"""

"""
The main boolean unit the communication between the classes.
"""
class Message():
    def __init__(self, message):
        self.message = message

"""
Possitive answer.
"""
# REVIEW: Decide if the ok has to have a code ( I guess not)
class OK(Message):
    def __init__(self, message='OK', code=0):
        super()__init__(message)

"""
Negative answer
params:
    message (string): The message to be shown to the user.
    code (int): the error code.
"""
# TODO: define a set of error statuses.
class ERROR(Message):
    def __init__(self, message, code=0):
        super()__init__(message)

        if (code):
            self.code = code
