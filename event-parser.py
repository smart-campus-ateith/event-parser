from flask import Flask, request
from flask_restful import Resource, Api
from json import loads, dumps
from flask_jsonpify import jsonify
from model.job.job import Job
from lib.queue.queue import Queue
from model.event.event import Event
from model.request.request import Request
from exceptions.bad_request import BadRequestException


import os
class EventParser():
    def __init__(self):
        self.queue = Queue()
        self.buffer  = []
        self.app  = Flask('Event parser')
        self.api = Api(self.app)
        app = self.app

        '''
        Creates a request object and inits it's life cycle.

        post fields:
            project_id
            device
            group
            sensor
            value
            received_time
        '''
        @app.route('/', methods=['POST'])
        def handle_request():
            ustr_to_load = str(request.data, 'utf-8')
            body = loads(ustr_to_load)
            self.validate_request(['project_id', 'device', 'group', 'sensor', 'value', 'received_time'], body)
            userRequest = Request(body['project_id'], body['device'], body['group'], body['sensor'], body['value'], body['received_time'])
            userJob = Job(userRequest)

            # TODO: Add job to the queue
            if self.canCreateJob():

                # TODO: add back to queue
                '''
                Completes the job life cycle.
                '''

                while userJob.execute():
                    pass
            else:
                addToQueue(userJob)
            print ('OK')
            return "ok"

    '''
    Searches a request for required fields
    '''
    def validate_request(self, required_fields, request_body):
        validates =  all([field in request_body for field in required_fields])

        if not validates:
            raise BadRequestException

    '''
    # TODO: determine if the machine is able to handle another task
    '''
    def canCreateJob(self):
        return True

    '''
    # TODO: Add to a temporary queue of items.
    '''
    def addToQueue(self, job):
        pass

if __name__ == '__main__':
    ep = EventParser()
    ep.app.run(host="0.0.0.0", port='5000', debug = True)
