'''
Request schema
'''

# the create table string for actions.
actions_schema = '''(
        id INT NOT NULL PRIMARY KEY,
        NAME STRING NOT NULL,
        type STRING NOT NULL
    )
    '''

# The create table string for jobs.
jobs_schema = ''' (
    job_id INTEGER PRIMARY KEY,
    created_at DATETIME NOT NULL ,
    project_id INTEGER NOT NULL,
    group_id INTEGER NOT NULL,
    sending_device INTEGER NOT NULL,
    sensor_id INTEGER NOT NULL,
    received_time DATETIME NOT NULL,
    value STRING NOT NULL


)'''
