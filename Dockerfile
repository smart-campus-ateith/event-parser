FROM alpine:latest

RUN apk add --update python3
RUN apk add py-pip
RUN pip3 install --upgrade pip

WORKDIR /app
COPY  requirements.txt /app
RUN pip3 install -r /app/requirements.txt

COPY . /app

ENV DATABASE_NAME=.event-parser.sqlite
RUN ["python3", "/app/scripts/db_init.py"]
ENTRYPOINT ["python3", "/app/event-parser.py"]
