"""
Job is the complete of a request from the receive action until the publication.

properties:
    lifecycle (array of Action): All the steps that the Job has to execute.
    history (array of Event): The status of the actions that should be already
                              executed.

"""

from model.action.action import Create, Process, Store, Publish


class Job(object):
    def __init__(self, request):
        self.history = []
        self.request = request
        self.lifecycle = self.parseLifecycle()
        self.actions = self.getLifecycle()

    def __str__(self):
        line = ""
        line += "this is a line"
        return line

    '''
    Bundles the data of the job
    '''
    def getData(self):

        data = {
            'created_at': self.request.created_at,
            'project_id': self.request.project_id,
            'sending_device': self.request.device,
            'group_id': self.request.group,
            'sensor_id': self.request.sensor,
            'received_time': self.request.received_time,
            'value': self.request.value,
        }

        return data

    """
    creates the lifcycle for a job.
    """
    # NOTE: For now use the simplest lifecycle.
    def parseLifecycle(self):
        # NOTE: abstract me.
        # lifecycle = {
        #     'create': "default",
        #     'process': None,
        #     'store': "default"
        #     'publish': None
        # }

        items = self.request.getLifecycle()
        lifecycle = []
        # return
        # TODO: abstract this
        # NOTE: I should call getCreacte('default') or anything else
        if len(items) > 0  and items[0]:
            lifecycle.append(Create(self))
        if len(items) > 1  and items[1]:
            lifecycle.append(Process(self))
        if len(items) > 2  and items[2]:
            lifecycle.append(Store(self))
        if len(items) > 3  and items[3]:
            lifecycle.append(Publish(self))

        return lifecycle

    """
    Yields the next action to be dispatched.
    """
    def getLifecycle(self):
        actions = self.lifecycle

        for action in actions:
            yield action

    """
    Execute the next step of it's lifecycle.
    """
    def execute(self):
        try:
            action = next(self.actions)
            action.dispatch()
            return True
        except StopIteration:
            return False
