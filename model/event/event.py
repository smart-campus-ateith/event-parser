'''
Event is the main unit of the event parser.
It is a wrapper for the data and the meta data of a request.
Or else a record of what is done.
'''
class Event():
    def __init__(self):#, content, time, action):
        print ('I was created')
        # self.action = action
        # # REVIEW:
        # self.project = ''
        # self.content = content
        # self.timestamp = time

    def __repr__(self):
        return '[{}] {}'.format(self.timestamp, self.content)

    def dispatch(self):
        pass

    def wait(self):
        pass

    '''
    Responds to GET request.
    Implements the `get` function of the `Resource` class.
    '''
    def get(self):
        data = {
            "0": "this is a line",
            "1": "this is a another line",
            "2": "guess what? Yet another line"
        }

        return jsonify(data)


    '''
    Responds to POST requests.
    Implements the `post` function if the `Resource` class.
    Should create an event.
    '''
    def post(self):
        data = {
            '0': 'hello',
            '1': 'hello2'
        }

        return jsonify(data)
