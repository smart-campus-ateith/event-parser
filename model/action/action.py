# TODO: define all types of possible actions
# should be to send information, to parse from app or sensor and...
"""
The lifecycle of the jobs is represented as a set of actions.
Each steps describes one step of.

These classes should be able to be extended to serve different
purposes.
"""

from lib.db.database import DBManager
from lib.db.external_db import ExternalDatabase

"""
Root class of the action family.
"""
class Action(object):
    def __init__(self):
        pass

    # TODO: ensure that is implemented.
    """
    Calls the action.
    """
    def dispatch(self):
        pass

"""
Creates an Event for the job and stores is at the queue.
"""
class Create(Action):
    def __init__(self, job):
        self.job = job

    def dispatch(self):
        db = DBManager()
        data = self.job.getData()
        res = db.insert('jobs', data)
        self.job.id = res

"""
Executes the process action of the job.
"""
# NOTE: Expected to send it to an external app/script for
#       processing.
class Process(Action):
    def __init__(self, job):
        pass

"""
Executes the store process of the job.
"""
# NOTE: In practice this is an abstraction over the save to the database procedure.
class Store(Action):
    def __init__(self, job):
        self.job = job

    def dispatch(self):
        db = ExternalDatabase()
        db.store_value(self.job.getData())

"""
Reminder for future implementation.
In case of live updates and MQTT support.
"""
class Publish(Action):
    def __init__(self, job):
        pass

# NOTE: A default action should be
# create a simple Event object and sore it to the database.
# Leave the implementation of the rest abstraction for later.

# NOTE: These classes should be used as phases and not as actual implementation
# of the actions at the end of the day. You can ignore this note for now.
