"""
Script
initializes the local database for the event parser.
"""


# NOTE: proper db name
# .__name__.sqlite

import os
import re
import sys
import sqlite3
# from config.db_schema import  actions_schema, jobs_schema
# from exceptions.invalid_database_name import InvalidDatabaseName

# HACK: Add manually the variables that they should be imported
actions_schema = '''(
        id INT NOT NULL PRIMARY KEY,
        NAME STRING NOT NULL,
        type STRING NOT NULL
    )
    '''

# The create table string for jobs.
jobs_schema = ''' (
    job_id INTEGER PRIMARY KEY,
    created_at DATETIME NOT NULL ,
    project_id INTEGER NOT NULL,
    group_id INTEGER NOT NULL,
    sending_device INTEGER NOT NULL,
    sensor_id INTEGER NOT NULL,
    received_time DATETIME NOT NULL,
    value STRING NOT NULL


)'''



"""
Ιnitializes the database
"""
def init_database(database_name):
    conn = sqlite3.connect(database_name)
    c = conn.cursor()

    result = c.execute('create table actions ' + actions_schema)
    result = c.execute('create table jobs ' + jobs_schema)
    conn.commit();


"""
Drop all the database data and delete the database
"""
def delete_database(database_name):
    conn = sqlite3.connect(database_name)
    c = conn.cursor()

    c.execute('drop table actions')
    c.execute('drop table jobs ')
    conn.commit();

"""
Tests if the database name follows the allowed pattern.
Used for the gitignore
"""
def validate_db_name(database_name):
    pattern = re.compile('^\..+\.sqlite$')
    match = pattern.match(database_name)

    if match is None:
        raise InvalidDatabaseName

# TODO: throw exception if there is no env variable.
database_name = os.environ['DATABASE_NAME']

try:
    delete_database(database_name)
    print ('database droped - OK')
except:
    print ('database drop - ERROR')


validate_db_name(database_name)
# TODO: Use a proper logger
try:
    init_database(database_name);
    print('DATABASE BOOTSTRAP - OK')
except:
    print ('DATABASE BOOTSTRAP - ERROR')
    sys.exit(1)
